//
//  DetailImageViewController.m
//  tableTest
//
//  Created by Tester on 3/15/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "DetailImageViewController.h"

@interface DetailImageViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation DetailImageViewController

- (void) configureWithName: (NSString*) name andImageName: (NSString*) imageName {
    self.nameLabel.text = name;
    UIImage* img = [[UIImage alloc] init];
    img = [UIImage imageNamed:imageName];
    self.imageView.image = img;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
}

@end
