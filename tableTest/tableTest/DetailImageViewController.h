//
//  DetailImageViewController.h
//  tableTest
//
//  Created by Tester on 3/15/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailImageViewController : UIViewController

- (void) refreshImageAndName;

@end
