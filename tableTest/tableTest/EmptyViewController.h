//
//  EmptyViewController.h
//  tableTest
//
//  Created by Tester on 3/16/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyViewController : UIViewController

@property (strong, nonatomic) NSString* carName;
@property (strong, nonatomic) NSString* carImageName;

@end
