//
//  DetailedViewController.m
//  tableTest
//
//  Created by Tester on 3/15/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "DetailedViewController.h"

@interface DetailedViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *nameField;

@property (strong, nonatomic) NSString* carName;
@property (strong, nonatomic) UIImage* carImage;

@end

@implementation DetailedViewController

- (void) configureWithName: (NSString*) name andImageName: (NSString*) imageName {
    self.carName = name;
    UIImage* img = [[UIImage alloc] init];
    img = [UIImage imageNamed:imageName];
    self.carImage = img;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.imageView.image = self.carImage;
    self.nameField.text = self.carName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
