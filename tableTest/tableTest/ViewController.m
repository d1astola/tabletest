//
//  ViewController.m
//  tableTest
//
//  Created by Tester on 3/15/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "ViewController.h"
#import "DetailImageViewController.h"
#import "DetailedViewController.h"
#import "EmptyViewController.h"

@interface ViewController ()

@property (strong, nonatomic) NSArray* dataArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.dataArray = [[NSArray alloc] initWithObjects: @"Model S", @"Model X", @"Roadster", @"Model 3", @"Model R", @"Tesla", nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath* indexPath = [self.mainTableView indexPathForSelectedRow];
    DetailImageViewController* controller = segue.destinationViewController;
    NSString* stringForCell;
    NSInteger index = 6;
    if (indexPath.section == 0) {
        index = indexPath.row;
        stringForCell = [self.dataArray objectAtIndex: index];
    }
    else if (indexPath.section == 1) {
        index = indexPath.row + [self.dataArray count] / 2;
        stringForCell = [self.dataArray objectAtIndex: index];
    }
    
    NSString* path;
    if (index != 5) {
        path = [NSString stringWithFormat:@"%ld.jpg", index + 1];
    }
    else {
        path = [NSString stringWithFormat:@"%ld.png", index + 1];
    }
}

#pragma mark - Private Methods

- (NSString*) getCarName {
    NSIndexPath* indexPath = [self.mainTableView indexPathForSelectedRow];
    NSString* stringForCell;
    NSInteger index = 6;
    if (indexPath.section == 0) {
        index = indexPath.row;
        stringForCell = [self.dataArray objectAtIndex: index];
    }
    else if (indexPath.section == 1) {
        index = indexPath.row + [self.dataArray count] / 2;
        stringForCell = [self.dataArray objectAtIndex: index];
    }
    return stringForCell;
}

- (NSString*) getImageName {
    NSIndexPath* indexPath = [self.mainTableView indexPathForSelectedRow];
    NSInteger index = 6;
    if (indexPath.section == 0) {
        index = indexPath.row;
    }
    else if (indexPath.section == 1) {
        index = indexPath.row + [self.dataArray count] / 2;
    }
    NSString* path;
    if (index != 5) {
        path = [NSString stringWithFormat:@"%ld.jpg", index + 1];
    }
    else {
        path = [NSString stringWithFormat:@"%ld.png", index + 1];
    }
    return path;
}

#pragma mark - Table View Data Source

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.dataArray count] / 2;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* cellId = @"SimpleTableId";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
    }
    
    NSString* stringForCell;
    
    if (indexPath.section == 0) {
        stringForCell = [self.dataArray objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 1) {
        stringForCell = [self.dataArray objectAtIndex:indexPath.row + [self.dataArray count] / 2];
    }
    [cell.textLabel setText:stringForCell];
    return cell;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return @"Tesla 1";
    }
    else {
        return @"Tesla 2";
    }
}
/*
- (NSString*) tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return @"End list of Tesla 1";
    }
    else {
        return @"End list of Tesla 2";
    }
}
*/
#pragma mark - TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
//    DetailImageViewController* controller = [[DetailImageViewController alloc] initWithName:[self.dataArray objectAtIndex:indexPath.row]];
    //[];
//    [self.navigationController pushViewController:controller animated:YES];
//    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//    NSLog(@"Cell's section is: %@, row is: %ld, it value is: %@", indexPath.section, indexPath.row, cell.textLabel.text);
//    [self performSegueWithIdentifier:@"showDetailImage" sender:self];
    
    
//    DetailedViewController* dvc = [[DetailedViewController alloc] init];
//
//    [dvc configureWithName: [self getCarName] andImageName:[self getImageName]];
//
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    [self.navigationController pushViewController:dvc animated:YES];
//    [self presentViewController:dvc
//                       animated:YES
//                     completion:^{
//                         
//                     }];
    EmptyViewController* evc = [[EmptyViewController alloc] init];
    evc.carImageName = [self getImageName];
    evc.carName = [self getCarName];
    [self.navigationController pushViewController:evc animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
