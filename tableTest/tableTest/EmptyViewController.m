//
//  EmptyViewController.m
//  tableTest
//
//  Created by Tester on 3/16/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "EmptyViewController.h"

@interface EmptyViewController ()

@property (weak, nonatomic) UIImageView* imageView;
@property (weak, nonatomic) UILabel* carLabel;

@end

@implementation EmptyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 200)];
    UIImage* img = [[UIImage alloc] init];
    img = [UIImage imageNamed:self.carImageName];
    imageView.image = img;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageView];
    
    UILabel* nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 320, self.view.frame.size.width, 50)];
    nameLabel.text = self.carName;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:nameLabel];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
